# old-classification-using-local-binary-pattern-knn

Old Classification using Local Binary Pattern & KNN
Run Program LBP Code First
in Google Colab

```
!python path/LBP_testing_file.py  --testing image/testing
!python path/LBP_training_file.py  --training image/training
```

you can change directory for saving csv file before running LBP testing or training in LBP code

```
        with open('LBP 4,1\Test4,1.csv', 'a', newline=None) as file:
            fieldnames = ['Image', 'Features', 'Label']
            writer = csv.DictWriter(file, fieldnames=fieldnames)
            writer.writerow(
                {'Image': namafile, 'Features': histogramMBLP, 'Label': label})
        print("Data Test Ke = ", namafile)
```

after everything are clear

just run KNN file with this code

`!python path/KNN_file.py`

note* u can just use right click on file.py in google colab to find path

Make sure to remove csv file before running LBP_testing_file or LBP_training_file




