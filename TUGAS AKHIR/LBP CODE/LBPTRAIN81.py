import cv2
from cv2 import cv2
import os
import os.path
import numpy as np
import csv
import pandas as pd
from imutils import paths
import argparse
import matplotlib
import matplotlib.pyplot as plt


def bandingPixel(gambar, center, x, y):
    n_banding = 0
    try:
        # hasil = gambar[x][y]-center
        # if hasil >= center:
        if gambar[x][y] >= center:
            n_banding = 1
    except:
        pass
    return n_banding


def riuLBP(gambar, x, y):
    "X = BARIS"

    "x-1, x-1, x-1"
    "x  , x  , x  "
    "x+1, x+1, x+1"

    "Y = KOLOM"

    "y-1, y, y+1"
    "y-1, y, y+1"
    "y-1, y, y+1"

    center = gambar[x][y]
    pixel_koordinat = []
    pixel_koordinat.append(bandingPixel(gambar, center, x-1, y+1))
    pixel_koordinat.append(bandingPixel(gambar, center, x, y+1))
    pixel_koordinat.append(bandingPixel(gambar, center, x+1, y+1))
    pixel_koordinat.append(bandingPixel(gambar, center, x+1, y))
    pixel_koordinat.append(bandingPixel(gambar, center, x+1, y-1))
    pixel_koordinat.append(bandingPixel(gambar, center, x, y-1))
    pixel_koordinat.append(bandingPixel(gambar, center, x-1, y-1))
    pixel_koordinat.append(bandingPixel(gambar, center, x-1, y))

    uniform = 0
    intensitas = 9
    for i in range(len(pixel_koordinat)):
        if pixel_koordinat[i] > pixel_koordinat[i-1] or pixel_koordinat[i] < pixel_koordinat[i-1]:
            uniform = uniform+1
    hitungangkasatu = pixel_koordinat.count(1)
    try:
        if uniform <= 2:
            intensitas = hitungangkasatu
    except:
        pass
    return intensitas


def main():

    data = []

    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--training", required=True,
                    help="path to the training images")
    args = vars(ap.parse_args())

    for imagePath in paths.list_images(args["training"]):
        image = cv2.imread(imagePath)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        height, width, channel = image.shape
        # img_lbp = np.zeros((height, width, 3), np.uint8)
        for i in range(0, height):
            for j in range(0, width):
                img_lbp = riuLBP(gray, i, j)
                data.append(img_lbp)

        # Mengubah menjadi numpy array
        datanp = np.array(data)
        # Kita reshape menjadi 200 x 200
        datareshape = datanp.reshape(200, 200)
        "y:y+h x:x+w"
        # "diagram gambar dari atas (ingat) x=0 y=0 mulai dari atas"

        cropped_1 = datareshape[0:0+100, 0:0+100]
        cropped_2 = datareshape[0:0+100, 100:100+100]
        cropped_3 = datareshape[100:100+100, 0:0+100]
        cropped_4 = datareshape[100:100+100, 100:100+100]
        a = np.hstack(cropped_1)
        b = np.hstack(cropped_2)
        c = np.hstack(cropped_3)
        d = np.hstack(cropped_4)

        histogram = []

        counts, bins, bars = plt.hist(a)
        # print(counts)
        histogram.append(counts)
        counts, bins, bars = plt.hist(b)
        # print(counts)
        histogram.append(counts)

        counts, bins, bars = plt.hist(c)
        # print(counts)
        histogram.append(counts)

        counts, bins, bars = plt.hist(d)
        # print(counts)
        histogram.append(counts)

        histogramMBLP = np.hstack(histogram)
        # print(histogramMBLP)

        label = imagePath.split(os.path.sep)[-2]
        namafile = imagePath.split(os.path.sep)[-1]

        # df = pd.DataFrame(histogramMBLP)
        # features = np.array(df.iloc[:, 0])
        with open('TUGAS AKHIR\LBP 8,1\Train8,1.csv', 'a', newline=None) as file:
            fieldnames = ['Image', 'Features', 'Label']
            writer = csv.DictWriter(file, fieldnames=fieldnames)
            writer.writerow(
                {'Image': namafile, 'Features': histogramMBLP, 'Label': label})
        print("Data Train Ke = ", namafile)
        histogram.clear()
        data.clear()


if __name__ == '__main__':
    main()
    
