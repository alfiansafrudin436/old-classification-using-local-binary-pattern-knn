import pandas as pd
import numpy as np
import random
import math
import json
import re
import numpy as np
from random import randrange
from random import seed
from math import sqrt
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import csv


def make_list(string_list_feature):
    clean_punct = string_list_feature.replace("[", "").replace("]", "")
    list_f = [float(x) for x in clean_punct.split()]
    return list_f


def euclidean_distance(x, y):
    distance = 0.0
    for i in range(len(x)-1):
        distance += (float(x[i]) - float(y[i]))**2
    return math.sqrt(distance)


kolom = ['File', 'Features', 'Label']
dataTrain = pd.read_csv(
    '/content/drive/My Drive/TUGAS AKHIR/LBP 16,2/Train16,2.csv', names=kolom, header=None)
dataTrain.Features = dataTrain.Features.apply(make_list)


kolom = ['File', 'Features', 'Label']
dataTest = pd.read_csv('/content/drive/My Drive/TUGAS AKHIR/LBP 16,2/Test16,2.csv',
                       names=kolom, header=None)
dataTest.Features = dataTest.Features.apply(make_list)

print(len(dataTest))

y_pred = []
y_true = []
X_train = []
X_test = []
X_train = np.array(dataTrain)
X_test = np.array(dataTest)
panjang_features = len(X_train[0, :]) - 1
# print(panjang_features)

K = 11
for i in range(0, len(dataTest)):
    Distance = []
    label = []
    print("Data Test Ke : ", i+1)
    # Menghitung Jarak Data
    for j in range(0, len(dataTrain)):
        x = X_test[:, 1][i]
        y = X_train[:, 1][j]
        Distance.append(euclidean_distance(x, y))
        label.append(X_train[j][panjang_features])
        distanceData = {'jarak': pd.Series(Distance),
                        'label': pd.Series(label)}
    # Mencari data terdekat dengan K
    df = pd.DataFrame(distanceData, columns=['jarak', 'label'])
    df = df.sort_values(by=['jarak'])
    df = df.head(K)

    # mengambil mayoritas dari label
    y_pred.append(df.groupby(["label"]).count().sort_values(by=['jarak']).
                  tail(1).index.get_level_values('label')[0])
    # Mendapatkan Label Sebenarnya
    y_true.append(X_test[i][panjang_features])

    namafile = dataTest['File']
    with open('/content/drive/My Drive/TUGAS AKHIR/EUCLEDEAN/K11(16,2).csv', 'a', newline=None) as file:
        fieldnames = ['Image', 'Prediksi', 'Target']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writerow(
            {'Image': namafile[i], 'Prediksi': y_pred[i], 'Target': y_true[i]})
    i += 1
# Mendapatkan Akurasi Dan Recall
recall = recall_score(y_true, y_pred, average='micro')
acc = accuracy_score(y_true, y_pred)
akurasi = acc*100
print(classification_report(y_true, y_pred))
print('Data Testing : %f' % len(dataTest))
print('Data Training: %f' % len(dataTrain))
print('Nilai K: %f' % K)
print('Accuracy: %f' % akurasi)
print("\n")

# prediksi = []
# asli = []

# for i in y_pred:
#     if i == "anak anak":
#         nilai = 0
#         prediksi.append(nilai)
#     if i == "remaja":
#         nilai = 1
#         prediksi.append(nilai)
#     if i == "dewasa":
#         nilai = 2
#         prediksi.append(nilai)
#     if i == "lansia":
#         nilai = 3
#         prediksi.append(nilai)
# for i in y_true:
#     if i == "anak anak":
#         nilai = 0
#         asli.append(nilai)
#     if i == "remaja":
#         nilai = 1
#         asli.append(nilai)
#     if i == "dewasa":
#         nilai = 2
#         asli.append(nilai)
#     if i == "lansia":
#         nilai = 3
#         asli.append(nilai)
# print("\nConfusion Matrix : ")
# confusiom = pd.DataFrame(
#     confusion_matrix(y_true, y_pred, labels=[
#                      'anak anak', 'remaja', 'dewasa', 'lansia']),
#     index=['TRUE:anak anak', 'TRUE:remaja', 'TRUE:dewasa', 'TRUE:lansia'],
#     columns=['PRED:anak anak', 'remaja', 'dewasa', 'lansia']
# )
# print(confusiom)

print("\nConfusion Matrix : ")

print(confusion_matrix(y_true, y_pred))
