import pandas as pd
import numpy as np
import random
import math
import json
import re
import numpy as np
from random import randrange
from random import seed
from math import sqrt
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import cv2
from cv2 import cv2
import csv
import glob
import matplotlib.pyplot as plt


def make_list(string_list_feature):
    clean_punct = string_list_feature.replace("[", "").replace("]", "")
    list_f = [float(x) for x in clean_punct.split()]
    return list_f


def euclidean_distance(x, y):
    distance = 0.0
    for i in range(len(x)-1):
        distance += (float(x[i]) - float(y[i]))**2
    return math.sqrt(distance)


kolom = ['File', 'Features', 'Label']
dataTrain = pd.read_csv(
    '/TA REVISI/LBP 16,2/Train16,2.csv', names=kolom, header=None)
dataTrain.Features = dataTrain.Features.apply(make_list)


kolom = ['File', 'Features', 'Label']
dataTest = pd.read_csv('/TA REVISI/LBP 16,2/Test.csv',
                       names=kolom, header=None)
dataTest.Features = dataTest.Features.apply(make_list)

print(len(dataTest))

y_pred = []
y_true = []
X_train = []
X_test = []
X_train = np.array(dataTrain)
X_test = np.array(dataTest)
panjang_features = len(X_train[0, :]) - 1
# print(panjang_features)

K = 1
for i in range(0, len(dataTest)):
    Distance = []
    label = []
    print("Data Test Ke : ", i+1)
    # Menghitung Jarak Data
    for j in range(0, len(dataTrain)):
        x = X_test[:, 1][i]
        y = X_train[:, 1][j]
        Distance.append(euclidean_distance(x, y))
        label.append(X_train[j][panjang_features])
        distanceData = {'jarak': pd.Series(Distance),
                        'label': pd.Series(label)}
    # Mencari data terdekat dengan K
    df = pd.DataFrame(distanceData, columns=['jarak', 'label'])
    df = df.sort_values(by=['jarak'])
    df = df.head(K)

    # mengambil mayoritas dari label
    y_pred.append(df.groupby(["label"]).count().sort_values(by=['jarak']).
                  tail(1).index.get_level_values('label')[0])
    # Mendapatkan Label Sebenarnya
    y_true.append(X_test[i][panjang_features])

    namafile = dataTest['File']
    namalabel = dataTest['Label']

i = 0
path = glob.glob("C:/TA REVISI/image/test/"+namalabel[i]+"/*.jpg")
for img in path:
    n = cv2.imread(img)
    cv2.putText(n, y_pred[i], (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
                1.0, (0, 0, 255), 3)
    plt.imshow(n)
    plt.show()
    i += 1    # for image in cv_img:
